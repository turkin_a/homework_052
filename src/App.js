import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Number from "./Number";

class App extends Component {
  state = {
    numbers: [0, 0, 0, 0, 0]
  };

  sorting = numbers => {
    let newArrayOfNumbers = [];

    for (let i = numbers.length; i > 0; i--) {
      let min = 0;

      for (let index in numbers) {
        if (numbers[index] < numbers[min]) min = index;
      }

      newArrayOfNumbers.push(numbers.splice(min, 1)[0]);
    }

    return newArrayOfNumbers;
  };

  getNumbers = () => {
    let numbers = [];

    do {
      let flag = true;
      let newNumber = Math.ceil(Math.random() * 36);

      for (let num of numbers) {
        if (newNumber === num) {
          flag = false;
          break;
        }
      }

      if (flag) numbers.push(newNumber);

    } while (numbers.length < this.state.numbers.length);

    numbers = this.sorting(numbers);
    this.setState({numbers});
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Homework #52</h1>
        </header>
        <div><button className="btn" type="button" onClick={this.getNumbers}>New numbers</button></div>
        <div className="numbers-block">
          <Number number={this.state.numbers[0]} />
          <Number number={this.state.numbers[1]} />
          <Number number={this.state.numbers[2]} />
          <Number number={this.state.numbers[3]} />
          <Number number={this.state.numbers[4]} />
        </div>
      </div>
    );
  }
}

export default App;
