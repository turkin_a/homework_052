import React from 'react';
import './Number.css';

const Number = (props) => {
  return (
    <div className="number">{props.number}</div>
  );
};

export default Number;